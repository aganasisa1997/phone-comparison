# Test assignment for a Front-end developer vacancy in "ООО Информационные системы и сервисы"
The project was completed according to the technical [task](https://docs.google.com/document/d/1y4Ulv0NKYBPcIimPrN1mFA2V7Xs7yaKNezkslai2RvY/edit?usp=sharing) .

The application is made with React, Redux.

To start, you need to install [JSON Server](https://github.com/typicode/json-server#simple-example) globally:

### `npm install -g json-server`

After that, go to the project folder in the console and start the JSON Server:

### `json-server --watch db.json --port 3004`

All data is stored in the file "db.json".\
Now the JSON Server is available at [http://localhost:3004/](http://localhost:3004/)


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

