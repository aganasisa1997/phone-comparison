import React from 'react';
import {Header} from './component/Header'
import {Compare} from './component/Compare'
import {Provider} from "react-redux";
import {store} from './store';

const App = () => {
    return (
        <Provider store={store}>
            <>
                <Header/>
                <Compare/>
            </>
        </Provider>
    )
}

export default App;
