import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

export const fetchProduct = createAsyncThunk(
    'product/fetchProduct',
async function (_, {rejectWithValue}) {
    try {
        const response = await fetch(`http://localhost:3004/products`)
        const data = await response.json();
        if (!Array.isArray(data)) {
            throw new Error("Сервер не доступен");
        }
        return data;
    } catch (error) {
        return rejectWithValue(error)
    }
    }
)

export const fetchSpecifications = createAsyncThunk(
    'product/fetchSpecifications',
    async function (_, {rejectWithValue}) {
        try {
            const response = await fetch(`http://localhost:3004/specificationsProduct`)
            const data = await response.json();
            return data;
        } catch (error) {
            return rejectWithValue(error)
        }
    }
)

interface ITitle {
    [key:string]: string
}

export interface ISpecificationsTel {
    title: ITitle,
    keys: string[]
}

export interface IProduct {
    id: string,
    name: string,
    preview: string,
    specifications: { [key:string]: string|boolean|number }
}

interface IStateProcuct {
    specificationsProduct: ISpecificationsTel,
    products: IProduct[],
    chooseProduct:string[],
    noChooseProduct:string[],
    chooseCount: number
    loadingProduct:boolean,
    loadingSpecifications: boolean,
    loadedProduct: boolean,
    loadedSpecifications: boolean,
    maxCount: 2|3|4|5|6,
    pageMobile: number
}

const initialState:IStateProcuct = {
    chooseCount: 3,
    specificationsProduct: {
        title: {},
        keys: []
    },
    products: [],
    chooseProduct: [],
    noChooseProduct: [],
    loadingProduct: false,
    loadedProduct: false,
    loadingSpecifications: false,
    loadedSpecifications: false,
    maxCount: 6,
    pageMobile: 0,
}


const productSlice = createSlice({
    name: 'product',
    initialState,
    reducers: {
        changeChooseProduct(state, action) {
            state.chooseProduct = state.chooseProduct.map((item) => item === action.payload.prevId? action.payload.newId:item);
            state.noChooseProduct = state.noChooseProduct.map(item => item === action.payload.newId? action.payload.prevId:item)
        },
        changeProductCount(state, action) {
            state.pageMobile = 0;
            const difference = action.payload.count - state.chooseCount;
            const addProductMas = difference > 0 ? state.chooseProduct : state.noChooseProduct;
            const removeProductMas = difference > 0 ? state.noChooseProduct : state.chooseProduct;
            for (let i =0;i<Math.abs(difference);i++) {
                const addProduct = removeProductMas.pop();
                if (typeof addProduct !== 'undefined') {
                    addProductMas.push(addProduct);
                } else break
            }

            state.chooseCount = action.payload.count;
        },
        changePageMobile(state, action) {
            state.pageMobile = action.payload;
        }
    },
    extraReducers: {
        [fetchProduct.pending.type]: (state) => {
            state.loadingProduct = true;
        },
        [fetchProduct.fulfilled.type]: (state, action) => {
            state.loadingProduct = false;
            state.products = action.payload;
            state.chooseProduct = [];
            state.noChooseProduct = [];
            state.loadedProduct = true;
            state.maxCount = action.payload.length <6? action.payload.length: 6;
            state.chooseCount = action.payload.length <3? action.payload.length: 3
            for(let i=0;i<action.payload.length;i++) {
                if (i < state.chooseCount) {
                    state.chooseProduct.push(action.payload[i].id);
                } else {
                    state.noChooseProduct.push(action.payload[i].id);
                }
            }

        },
        [fetchProduct.rejected.type]: (state) => {
            state.loadingProduct = false;
            state.loadedProduct = true;
        },
        [fetchSpecifications.pending.type]: (state) => {
            state.loadingSpecifications = true;
        },
        [fetchSpecifications.fulfilled.type]: (state, action) => {
            state.loadingSpecifications = false;
            state.loadedSpecifications = true;
            state.specificationsProduct = action.payload;
        },
        [fetchSpecifications.rejected.type]: (state) => {
            state.loadingSpecifications = false;
            state.loadedSpecifications = true;
        },
    }
})
export const {changeChooseProduct, changeProductCount, changePageMobile} = productSlice.actions;
export default productSlice.reducer
