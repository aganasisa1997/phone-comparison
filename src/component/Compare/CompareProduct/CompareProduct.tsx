import React, {ChangeEvent, useEffect} from 'react';
import styles from './compareproduct.module.css';
import {CSSProperties} from "react";
import {ProductPreview} from "./ProductPreview";
import {changePageMobile, IProduct, ISpecificationsTel} from "../../../store/slices/productSlice";
import {useAppSelector} from "../../../hooks/redux-hook";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../../../store";
import {PreviewProductLine} from "./PreviewProductLine";
import {SpecificationProductLine} from "./SpecificationProductLine";


export function CompareProduct() {
    const checkProductCount:number = useAppSelector(state => state.product.chooseCount);
    const [onlyChange, setOnlyChange] = React.useState(false);
    const products:IProduct[] = useAppSelector(state => state.product.products);
    const chooseProductId:string[] = useAppSelector(state => state.product.chooseProduct)
    const chooseProduct:IProduct[] = chooseProductId.map((id) => {
        let choose:IProduct = products[0];
        products.forEach(item => {
            if (item.id === id) choose = item
        })
        return choose;
    })
    const dispatch:AppDispatch = useDispatch();
    const [tableMod, setTableMod] = React.useState(window.matchMedia('(max-width: 1023px) and (min-width:768px)').matches);
    const [mobileMod, setMobileMod] = React.useState(window.matchMedia('(max-width:767px)').matches);
    let visualProductCount:number = checkProductCount;
    const visibleOnTablet = 3;
    const visibleOnMobile = 2;

    if (mobileMod) {
        visualProductCount = visualProductCount>visibleOnMobile?visibleOnMobile:visualProductCount;
    }  else if (tableMod) {
        visualProductCount = visualProductCount>visibleOnTablet?visibleOnTablet: visualProductCount;
    }

    const pageMobile = useAppSelector(state => state.product.pageMobile);
    const [visibleBtnPrev, setVisibleBtnPrev] = React.useState(pageMobile > 0);
    const [visibleBtnNext, setVisibleBtnNext] = React.useState(pageMobile < (checkProductCount - visualProductCount));

    const checkVisibleBtn = (pageValue:number = pageMobile) => {

        let vvv = checkProductCount;
        if (mobileMod) vvv = 2;
        if (tableMod) vvv = 3;
        if (pageValue === 0 && visibleBtnPrev) {
            setVisibleBtnPrev(false);
        } else if (pageValue > 0 && !visibleBtnPrev) {
            setVisibleBtnPrev(true);
        }

        if (pageValue === (checkProductCount-vvv) && visibleBtnNext) {
            setVisibleBtnNext(false);
        } else if (pageValue < (checkProductCount-vvv) && !visibleBtnNext){
            setVisibleBtnNext(true);
        }
    }
    const prevPage = () => {
        dispatch(changePageMobile(pageMobile-1));
      checkVisibleBtn(pageMobile-1);
    };
    const nextPage = () => {
        dispatch(changePageMobile(pageMobile+1));
        checkVisibleBtn(pageMobile+1);
    };
    useEffect(() => {
        checkVisibleBtn(0);
    },[checkProductCount])

    useEffect(() => {
        const changeSizeWindow = () => {
            if (window.matchMedia('(max-width: 1023px) and (min-width:768px)').matches) {
                setTableMod(true);
                setMobileMod(false);
            } else if (window.matchMedia('(max-width:767px)').matches){
                setTableMod(false);
                setMobileMod(true);
            } else {
                setTableMod(false);
                setMobileMod(false);
                dispatch(changePageMobile(0));
            }
        }

        window.addEventListener('resize', changeSizeWindow);

        return () => {
            window.removeEventListener('resize', changeSizeWindow);
        }
    }, []);

    useEffect(() => {
        checkVisibleBtn();
    },[mobileMod,tableMod]);


    const handleChange = (e:ChangeEvent<HTMLInputElement>) => {
        setOnlyChange(e.target.checked);
    }


    return (
        <div className={styles.wrapProduct}>
            <PreviewProductLine
                visualProductCount={visualProductCount}
                onlyChange={onlyChange}
                handleChange={handleChange}
                tableMod={tableMod}
                mobileMod={mobileMod}
                chooseProduct={chooseProduct}
            />
            {visibleBtnPrev && (<div className={styles.btnArrow+' '+styles._prev} onClick={prevPage}></div>)}
            {visibleBtnNext && (<div className={styles.btnArrow+' '+styles._next} onClick={nextPage}></div>)}
            <SpecificationProductLine
                tableMod={tableMod}
                mobileMod={mobileMod}
                onlyChange={onlyChange}
                visualProductCount={visualProductCount}
                chooseProduct={chooseProduct}
            />
        </div>
    );
}
