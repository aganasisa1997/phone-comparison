import React from 'react';
import styles from './productpreview.module.css';
import {Dropdown} from "../../../Dropdown";
import {changeChooseProduct} from "../../../../store/slices/productSlice";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../../../../store";
import {useAppSelector} from "../../../../hooks/redux-hook";

interface IProductPreview {
  data: {
      preview:string,
      id: string,
      name:string
  }
}
export function ProductPreview({data}:IProductPreview) {
    const [openDropdown, setOpenDropdown] = React.useState(false);
    const {noChooseProduct} = useAppSelector(state => state.product)
    const dispatch:AppDispatch = useDispatch();
    const closeDropdown = () => {
        setOpenDropdown(false);
    }

    const handleChangeProduct = (id:string) => {
        dispatch(changeChooseProduct({prevId:data.id, newId: id}));
        closeDropdown();
    }

  return (
      <div className={styles.previewWrap}>
          <div className={styles.top}>
              <div className={styles.img}>
                  <img src={data.preview} alt=""/>
              </div>
              {noChooseProduct.length !== 0 &&
                  (<div className={styles.wrapDropdown}>
                      <button className={styles.btnArrow} onClick={() => setOpenDropdown(true)}></button>
                      {openDropdown && (<Dropdown handleChangeProduct={handleChangeProduct}  onClose={closeDropdown}/>)}
                  </div>)
              }
          </div>
          <p className={styles.name}>{data.name}</p>
      </div>
  );
}
