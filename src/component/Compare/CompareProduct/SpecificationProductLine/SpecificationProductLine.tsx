import React, {CSSProperties} from 'react';
import styles from './specificationproductline.module.css';
import {useAppSelector} from "../../../../hooks/redux-hook";
import {IProduct, ISpecificationsTel} from "../../../../store/slices/productSlice";
interface ISpecificationProductLine {
    tableMod:boolean,
    mobileMod:boolean,
    onlyChange: boolean,
    visualProductCount: number,
    chooseProduct:IProduct[],
}

export function SpecificationProductLine({tableMod,mobileMod, onlyChange, visualProductCount, chooseProduct}:ISpecificationProductLine) {
    const pageMobile = useAppSelector(state => state.product.pageMobile);
    const specificationsTel:ISpecificationsTel = useAppSelector(state => state.product.specificationsProduct);
    const checkProductCount:number = useAppSelector(state => state.product.chooseCount);
    const style: CSSProperties = {gridTemplateColumns: `repeat(${tableMod || mobileMod || checkProductCount>4?visualProductCount:visualProductCount+1}, 1fr)`}

    const getSpec = (key:string) => {
        let node:React.ReactNode[] = [];
        let differences = onlyChange;
        let prevSP:string|number|boolean = key in chooseProduct[0].specifications ? chooseProduct[0].specifications[key] : '';
        for(let i=pageMobile;i<visualProductCount+pageMobile;i++) {
            let value:string|React.ReactNode = key in chooseProduct[i].specifications ? chooseProduct[i].specifications[key] : '';
            if (typeof value == 'boolean') {
                value = (<span className={!!value? styles.available : styles.nAvailable}></span>);
            }
            if (differences && prevSP !== chooseProduct[i].specifications[key]) {
                differences = false;
            }
            prevSP = chooseProduct[i].specifications[key];
            node.push(
                <div key={key+chooseProduct[i].id}><span>{value}</span></div>
            );
        }
        return [node, differences];
    }
    const getSpecLine = () => {
        const node: React.ReactNode[] = [];
        specificationsTel.keys?.forEach(key => {
            const title = key in specificationsTel.title ? specificationsTel.title[key] : '';
            const [nodeSp, differences] = getSpec(key);
            if (!differences) {
                node.push((
                    <div className={styles.rowSp} style={style} key={'sp'+key}>
                        <div className={styles.titleRow}><span>{title}</span></div>
                        {nodeSp}
                    </div>
                ));
            }
        });
        return node.length > 0 ? node: (<p className={styles.noDifferences}>Нет различий</p>);
    }

    return (
        <div className={styles.specificationsWrap + ' ' + (checkProductCount > 4 ? styles._bigChoose : '')}>
            <div className={styles.specifications}>
                {getSpecLine()}
            </div>
        </div>
    );
}
