import React, {ChangeEvent, CSSProperties} from 'react';
import styles from './previewproductline.module.css';
import {ProductPreview} from "../ProductPreview";
import {useAppSelector} from "../../../../hooks/redux-hook";
import {IProduct} from "../../../../store/slices/productSlice";
interface IPreviewProductLine {
    visualProductCount: number,
    onlyChange: boolean,
    handleChange: (e:ChangeEvent<HTMLInputElement>) => void,
    tableMod: boolean,
    mobileMod: boolean,
    chooseProduct:IProduct[]
}
export function PreviewProductLine({visualProductCount, onlyChange, handleChange, tableMod,mobileMod, chooseProduct}:IPreviewProductLine) {
    const pageMobile = useAppSelector(state => state.product.pageMobile);
    const checkProductCount:number = useAppSelector(state => state.product.chooseCount);
    const style: CSSProperties = {gridTemplateColumns: `repeat(${tableMod || mobileMod || checkProductCount>4?visualProductCount:visualProductCount+1}, 1fr)`}

    const gerPreviewProducts = () => {
        let node:React.ReactNode[] = [];
        for(let i=pageMobile;i<visualProductCount+pageMobile;i++) {
            node.push(<ProductPreview data={chooseProduct[i]} key={'preview' + chooseProduct[i].id}/>)
        }
        return node;
    }

    return (
        <div className={styles.previewProduct+' '+(checkProductCount > 4 ?styles._bigChoose:'')} style={style}>
            <div className={styles.fieldDif}>
                <label className={styles.checkbox}>
                    <input type="checkbox" checked={onlyChange} onChange={handleChange}/>
                    <span></span>
                    <p>Показать различия</p>
                </label>
            </div>
            {gerPreviewProducts()}
        </div>
    );
}
