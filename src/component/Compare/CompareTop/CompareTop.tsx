import React from 'react';
import styles from './comparetop.module.css';
import {useAppSelector} from "../../../hooks/redux-hook";
import {AppDispatch} from "../../../store";
import {useDispatch} from "react-redux";
import {changeProductCount} from "../../../store/slices/productSlice";

export function CompareTop() {
    const {chooseCount, maxCount} = useAppSelector(state => state.product);
    const dispatch:AppDispatch = useDispatch();
    const getButtonCount = () => {
        let content = [];
        for(let i=1;i<maxCount;i++) {
            content.push(<button className={(i+1)==chooseCount?styles._active:''} onClick={()=> dispatch(changeProductCount({count:i+1}))} key={'countproduct'+i}>{i+1}</button>)
        }
        return content;
    }
    return (
        <div className={styles.top}>
            <h1>Смартфоны</h1>
            <div className={styles.productCount}>
                <span>Отобразить товары:</span>
                {getButtonCount()}
            </div>
        </div>
    );
}
