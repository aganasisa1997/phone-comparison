import React, {useEffect} from 'react';
import styles from './compare.module.css';
import {CompareTop} from "./CompareTop";
import {CompareProduct} from "./CompareProduct";
import {useAppSelector} from "../../hooks/redux-hook";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../../store";
import {fetchProduct, fetchSpecifications} from "../../store/slices/productSlice";

import {Loader} from "../Loader";

export function Compare() {
    const loadingProduct = useAppSelector(state => state.product.loadingProduct);
    const loadedProduct = useAppSelector(state => state.product.loadedProduct);
    const loadingSpecifications = useAppSelector(state => state.product.loadingSpecifications);
    const loadedSpecifications = useAppSelector(state => state.product.loadedSpecifications);
    const dispatch:AppDispatch = useDispatch();

    useEffect(() => {
        if (!loadingProduct && !loadedProduct) {
            dispatch(fetchProduct());
        }
    }, [loadedProduct, loadingProduct]);

    useEffect(() => {
        if (!loadingSpecifications && !loadedSpecifications) {
            dispatch(fetchSpecifications());
        }
    }, [loadedSpecifications, loadingSpecifications]);

    return (
        <div className={styles.container}>
            {loadedProduct ? (
                <>
                    <CompareTop />
                    <CompareProduct/>
                </>
                ) : (<Loader></Loader>)}
        </div>
    );
}
