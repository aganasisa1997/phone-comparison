import React, {CSSProperties, useEffect} from 'react';
import styles from './dropdown.module.css';
import {useAppSelector} from "../../hooks/redux-hook";

import {ArrowIcon} from "../../svg";
import {IProduct} from "../../store/slices/productSlice";

interface IDropdown {
    onClose: () => void,
    handleChangeProduct: (id:string) => void
}


export function Dropdown({onClose, handleChangeProduct}:IDropdown) {
    const [search, setSearch] = React.useState('');
    const [notChooseProduct, setNotChooseProduct] = React.useState<IProduct[]>([]);
    const [conclusionProduct, setConclusionProduct] = React.useState<IProduct[]>([]);
    const ref = React.useRef<HTMLDivElement>(null);
    const [addClass, setAddClass] = React.useState('');

    const products = useAppSelector(state=> state.product.products);
    const noChooseProduct = useAppSelector(state => state.product.noChooseProduct);
    let style: CSSProperties = {overflowY: conclusionProduct.length > 3 ? 'auto': 'hidden'}

    const getNotChooseProduct = () => {
        return products.filter(product => {
            let notChoose = false;
            noChooseProduct.forEach((id) => {
                if (id === product.id) {
                    notChoose = true;
                }
            })
            return notChoose;
        });
    }
    const searchProduct = (searchText:string) => {
        return notChooseProduct.filter(product => product.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0)
    }

    const ChangeSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
        setConclusionProduct(searchProduct(event.target.value));
    }

    React.useEffect(() => {
        function handleClick(event:MouseEvent) {
            if (event.target instanceof Node && !ref.current?.contains(event.target)) {
                onClose();
            }
        }

        document.addEventListener('mousedown', handleClick);

        setNotChooseProduct(getNotChooseProduct());
        setConclusionProduct(getNotChooseProduct());
        return () => {
            document.removeEventListener('mousedown', handleClick);
        }
    }, [])

    useEffect(() => {
        const chechPosition = () => {
            const positionDropdown = ref.current?.getBoundingClientRect();
            const screenWidth = window.screen.width;
            if (typeof positionDropdown !== 'undefined' && positionDropdown.right > screenWidth) {
                setAddClass(styles._right);
            }
        }
        if (typeof ref !== 'undefined') {
            chechPosition();
            window.addEventListener('resize', chechPosition);
        }

        return () => {
            if (typeof ref !== 'undefined') {
                window.removeEventListener('resize', chechPosition);
            }
        }
    }, [ref]);

    return (
        <div className={styles.dropdown+' '+addClass} ref={ref}>
            {noChooseProduct.length > 3 && (
                <label className={styles.field}>
                    <input type="text" value={search} onChange={ChangeSearch} placeholder='Поиск' />
                </label>
            )}
            <div className={styles.scrollWrap} style={style}>
                <ul>
                    {conclusionProduct.map(item => {
                        return (<li key={'se'+item.id} className={styles.item}>
                            <button className={styles.btnChoose} onClick={() => {
                                handleChangeProduct(item.id)
                            }}>
                                <ArrowIcon />
                            </button>
                            <div className={styles.wrapImg}>
                                <img src={item.preview} alt=""/>
                            </div>
                            <p>{item.name}</p>
                        </li>);
                    })}
                </ul>
            </div>
        </div>
    );
}
