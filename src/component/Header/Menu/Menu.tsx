import React from 'react';
import styles from './menu.module.css';
import {UserIcon} from "../../../svg";


export function Menu() {
    const [mMenuOpen, setMMenuOpen] = React.useState<boolean>(false);

    return (
        <>
            <div className={styles.container}>
                <div className={styles.wrapMobile + ' ' + (mMenuOpen ? styles._open : '')}>
                    <div className={styles.menu}>
                        <a href="#">Сравнение</a>
                    </div>
                </div>
                <a href="#lk" className={styles.lk}>Личный кабинет
                    <UserIcon/>
                </a>
            </div>
            <button
                className={styles.btnMenu + ' ' + (mMenuOpen ? styles._open : '')}
                onClick={() => {
                        setMMenuOpen(!mMenuOpen)
                    }
                }>
                <span></span>
                <span></span>
                <span></span>
            </button>
        </>
    );
}
