import React from 'react';
import styles from './header.module.css';
import {Menu} from "./Menu";

export function Header() {
  return (
      <header className={styles.header}>
            <div className={styles.container}>
                <a href="#catalog" className={styles.title}>Каталог</a>
                <Menu />
            </div>
      </header>
  );
}
